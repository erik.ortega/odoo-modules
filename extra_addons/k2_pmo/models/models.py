# -*- coding: utf-8 -*-

# from odoo import models, fields, api


# class prueba(models.Model):
#     _name = 'prueba.prueba'
#     _description = 'prueba.prueba'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100


from odoo.exceptions import ValidationError
from tkinter import *
from tkinter import messagebox as MessageBox
import requests, json, logging
from odoo import models, fields, api

_logger = logging.getLogger(__name__)

class pricing(models.Model):
    _name = "k2_pmo.pricing_case"
    _description = "formulario de pricing case"

    consulting_area = fields.Selection(selection=[("enterprise_application","Enterprise Application"),
    ("custom_sw_dev","Custom Software Development"),("app_manag_services","Application Management Services")])
    client = fields.Char(string="Client" , help="nombre del cliente")
    project_name = fields.Char(string="Project Name")
    project_type = fields.Selection(selection=[("fixed_price","Fixed Price"),("time_and_materials","Time and Materials")])
    start_date = fields.Date(string="Start Date") 
    end_date = fields.Date(string="End Date") 
    country = fields.Many2one('res.country', string="country")
    legal_entity = fields.Selection(selection=[("k2_mexico","K2 Partnering Mexico")])
    currency = fields.Selection(selection=[("MXN","MXN"),("USD","USD")])
    
    # - Checkboxs
    usd_exchange = fields.Boolean()
    usd_exchange_value = fields.Float()
    @api.onchange('usd_exchange')
    def _onchange_usd_exchange(self):
        if self.usd_exchange == False:
            self.usd_exchange_value = 0
    base_days = fields.Boolean()
    base_days_value = fields.Float()
    @api.onchange('base_days')
    def _onchange_base_days(self):
        if self.base_days == False:
            self.base_days_value = 0
    
    # - relaciones entre tablas
    pricing_case_roles_id = fields.One2many("k2_pmo.pricing_case.roles", "pricing_case_id", string="Pricing Case Roles")
    pricing_case_milestone_id = fields.One2many("k2_pmo.pricing_case.milestone", "milestone_id", string="Pricing Case Milestones")

class roles(models.Model):
    _name = "k2_pmo.pricing_case.roles"
    _description = "Formularios de roles para pricing Case"

    contract_type =  fields.Selection(selection=[("payroll_100","Payroll 100%")])
    consultant_role = fields.Char()
    total_project_hours = fields.Integer()
    monthly_cost = fields.Float()
    computer_equipment = fields.Selection([("default", "Category")])
    
    # - Model relation
    pricing_case_id = fields.Many2one("k2_pmo.pricing_case")

    # - Medical insurance info
    gender = fields.Selection([("1","Female"),("0","Male")])
    age_range = fields.Selection([("category_1","21 - 30")])
    # -- Checkboxes
    pmo_costXhour = fields.Boolean()
    pmo_costXhour_value = fields.Float()
    @api.onchange('pmo_costXhour')
    def _onchange_pmo_costXhour(self):
        if self.pmo_costXhour == False:
            self.pmo_costXhour_value = 0
    sla_costs = fields.Boolean()
    sla_costs_value = fields.Float()
    @api.onchange('sla_costs')
    def _onchange_sla_costs(self):
        if self.sla_costs == False:
            self.sla_costs_value = 0
    value_added_costs = fields.Boolean()
    value_added_costs_value = fields.Float()
    @api.onchange('value_added_costs')
    def _onchange_value_added_costs(self):
        if self.value_added_costs == False:
            self.value_added_costs_value = 0
    travel_and_expenses = fields.Boolean()
    travel_and_expenses_value = fields.Float()
    @api.onchange('travel_and_expenses')
    def _onchange_travel_and_expenses(self):
        if self.travel_and_expenses == False:
            self.travel_and_expenses_value = 0
    risk_management_factor = fields.Boolean()
    risk_management_factor_value = fields.Float()
    @api.onchange('risk_management_factor')
    def _onchange_risk_management_factor(self):
        if self.risk_management_factor == False:
            self.risk_management_factor_value = 0
    tcv_bond = fields.Boolean()
    tcv_bond_value = fields.Float()
    @api.onchange('tcv_bond')
    def _onchange_tcv_bond(self):
        if self.tcv_bond == False:
            self.tcv_bond_value = 0

    # View info
    hourly_cost = fields.Float()
    daily_cost = fields.Float()
    sales_price = fields.Float()
    nfi = fields.Float('NFI', digits=(12,6))
    subtotal = fields.Float()
    
    #relaciones entre tablas
    pricing_case_id = fields.Many2one("k2_pmo.pricing_case", string="Pricing Case")
 
    
    #functions

    @api.model
    def create(self, vals):
        result = super(roles, self).create(vals)
        # do what you want
        urlTaxISR = "https://1b2ccht8c2.execute-api.us-east-2.amazonaws.com/Testing/pricing/calc-main"
        payload = '{"staffing": [{"id": "1","profile": "Lider Proyecto 1","totalHrsWork": 240,"costMontPrice": 52892.9099919,"salePriceHour": 700,"typePayRoll": "nomina","riskAplica": true,"comisionK2": 0,"categoria": "B","genero": "","edad": "","adders": 0.01,"riskPercent": 0.03,"upLiftK2": 0.34,"porcentajeNegociacion": 0,"UMA": 89.62,"salarioMinimo": 141.7,"diasMes": 30.4,"factor": 1.0452,"porcentajeISN": 0.03,"tipoCambio": 21,"monthDays": 168,"dayxMonth": 21,"pmoCost": 57.14,"travelExpense": 0,"deposite": 0,"valueAgregate": 0}]}'
        
        try:    
            k2CalcJson = {}
            if 'staffing' not in k2CalcJson:
                response = requests.post(urlTaxISR, json=json.loads(payload))
                k2CalcJson = json.loads(response.content)
            
            k2CalcList = k2CalcJson['staffing']
            k2calcDict = k2CalcList[0]


            if (response.status_code == 200):
                result.write(
                    {"hourly_cost" : k2calcDict['costHour'],
                    "nfi" : k2calcDict['margenRealNFI'],
                    "sales_price" : k2calcDict['monthPriceSale']}
                )
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            
            #MessageBox.showwarning("Alerta", "No se pudo establecer conexión con el servidor. Intentelo mas tarde.")    
            raise ValidationError("No se pudo establecer conexión con el servidor. \n" + message + "\n Content not found: \n"+str(response)+"\n"+str(response.content))
        return result

    class milestone(models.Model):
        _name = "k2_pmo.pricing_case.milestone"
        _description = "Formularios de milestones para pricing Case"

        deliverable_name = fields.Char()
        delivery_date = fields.Date()
        invoice_date = fields.Date()
        percentage = fields.Float()
        amount = fields.Float()

        # relation Between Models
        milestone_id = fields.Many2one("k2_pmo.pricing_case", string="Pricing Case")
